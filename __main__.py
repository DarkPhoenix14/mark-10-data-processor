from os import chdir
from glob import glob
import numpy as np
import pandas as pd
import math as m
import re
import logging
from scipy import stats
from pathlib import Path

logger = logging.getLogger(name="Mark 10 Processor")


def read(path: Path, method: str, area: float, load_units="N", displacement_units="mm", fixed=None) -> dict:
    """
    reads in values and calculates stress and strain
    :param path: file path
    :param method: "SUB"  # subtracting subsequent values from the first and dividing by the first
                   "FIXED"  # subtract all values from a fixed number and divide by that fixed number
    :param area: area of compression or tension
    :param load_units: units used for load
    :param displacement_units: unit used for movement
    :param fixed: height of object (for "FIXED" method only)
    :return: data: stress, strain, and young's modulus for each type
    """
    data = {}
    # get a list of files
    try:
        chdir(path)
    except FileNotFoundError:
        logger.error("Invalid File Path")
        return 1
    file_list = glob("[!~$]*.xlsx")
    if file_list is None:
        logger.error("No Data Found")
        return 1

    for file in file_list:
        # extract information from file name
        parts = re.split('(\d+)', file)
        title = parts[0][:len(parts[0]) - 1]
        type = int(parts[1])
        cat = tuple([title, type])

        # initialize category if necessary
        if cat not in data.keys():
            data[cat] = {
                "Stress": [],
                "Strain": [],
                # "Young's": [] ::: Old Method :::
            }

        # grab alpha code and name columns
        poly = parts[2].replace(".xlsx", '')
        column_stress = "Stress " + poly
        column_strain = "Strain " + poly

        # read in file
        source = pd.ExcelFile(file)
        parsed = pd.io.excel.ExcelFile.parse(source, "Sheet2")
        stress = pd.DataFrame({column_stress: parsed['Load [{}]'.format(load_units)]})
        strain = pd.DataFrame({column_strain: parsed['Travel [{}]'.format(displacement_units)]})

        # calculate stress
        stress[column_stress] = stress[column_stress].divide(area)

        # calculate strain
        if "SUB" in method.upper():
            strain[column_strain] = strain[column_strain].apply(
                lambda x: abs((strain[column_strain].values[0] - x) / (strain[column_strain].values[0])))
        elif "FIXED" in method.upper() and fixed is not None:
            strain[column_strain] = strain[column_strain].apply(lambda x: abs((x - fixed) / float(fixed)))
        else:
            logger.error("No Recognized Strain Method or Fixed Value Unspecified")
            logger.error("Strain Method:{} Fixed Value:{}".format(method, fixed))
            return 1

        # ::: Old Method :::
        # calculate Young's Modulus
        # youngs = stress.div(strain.values)
        # youngs.columns = ["Young's " + poly]

        # add individual file stress strains to larger grouping
        data[cat]["Stress"].append(stress)
        data[cat]["Strain"].append(strain)
        # data[cat]["Young's"].append(youngs)

    # Resize data to be all the same length
    for key in data.keys():
        for subkey in data[key].keys():
            data[key][subkey] = pd.concat(data[key][subkey], axis=1).dropna(axis=0)

    return data


def process(data: dict) -> dict:
    """
    processes data and calculates averages and standard error and young's modulus
    :param data: stress, strain, and young's modulus for each type
    :return: data with calculations completed
    """
    # get lists that are repeatedly iterated over
    categories = list(data.keys())
    d_types = list(data[categories[0]].keys())

    # calculate Young's
    for category in categories:
        # initialize Young's Category
        data[category]["Young's"] = {
            "Young's": [],
            "Intercept": [],
            "r^2": [],
            # Unused but here because these are returned but are empty because a the stats package always returns these
            # "p_value": [],
            # "std_error": [],
            "part_names": [],
        }
        # grab stress strain for each part
        for i in range(data[category]["Stress"].shape[1]):
            # do linear regression
            slope, intercept, r_value, p_value, std_err = stats.linregress(
                data[category]["Strain"].iloc[:, [i]].values.T.tolist(),
                data[category]["Stress"].iloc[:, [i]].values.T.tolist())

            # add values to data
            data[category]["Young's"]["Young's"].append(slope)
            data[category]["Young's"]["Intercept"].append(intercept)
            data[category]["Young's"]["r^2"].append(r_value ** 2)
            parts = data[category]["Strain"].columns[i].split()
            data[category]["Young's"]["part_names"].append(parts[1])
            # data[category]["Young's"]["p_value"].append(p_value)
            # data[category]["Young's"]["std_error"].append(std_err)

        # convert to dataframe
        data[category]["Young's"] = pd.DataFrame.from_dict(data[category]["Young's"])
        # change indexes to be part names
        data[category]["Young's"].index = data[category]["Young's"]["part_names"].tolist()
        data[category]["Young's"] = data[category]["Young's"].drop("part_names", axis=1)

        # Calculate mean and standard error and add to data
        means = pd.DataFrame(data[category]["Young's"].mean(axis=0)).T
        means.index = ["Mean"]
        se = pd.DataFrame(data[category]["Young's"].std(axis=0).divide(m.sqrt(data[category]["Young's"].shape[1]))).T
        se.index = ["Standard Error"]
        data[category]["Young's"] = data[category]["Young's"].append(means, ignore_index=False)
        data[category]["Young's"] = data[category]["Young's"].append(se, ignore_index=False)

    for category in categories:
        for d_type in d_types:
            # calculate mean and standard error and add to data
            means = data[category][d_type].mean(axis=1)
            se = data[category][d_type].std(axis=1).divide(m.sqrt(data[category][d_type].shape[0]))
            data[category][d_type][d_type + " Mean"] = means
            data[category][d_type][d_type + " SE"] = se

            # Remove calculation errors e.g. divide by zero
            data[category][d_type] = data[category][d_type].replace([np.inf, -np.inf], np.nan)
            data[category][d_type] = data[category][d_type].dropna(axis=0)

    return data


def write(data: dict, path: Path, name="Complied.xlsx", reading_numbers=False):
    """
    Writes data to file
    :param data: data from process
    :param path: path to write to
    :param name: file name
    :param reading_numbers: add a column for reading number
    :return: boolean whether function successfully wrote
    """
    # Change file location
    try:
        chdir(path)
    except FileNotFoundError:
        logger.error("Invalid File Path")

    # get all known categories and initialize fields
    groups = list(data.keys())
    comb = {}
    young = []
    for group in groups:
        # combine stress and strain into one dataframe
        comb[group] = pd.concat([data[group]["Stress"], data[group]["Strain"]], axis=1)

        # Label column headers by type
        data[group]["Young's"].columns = [str(col) + " " + group[0] + " " + str(group[1]) for col in
                                          data[group]["Young's"].columns]
        young.append(data[group]["Young's"])

    # combine all young's modulus into one dataframe
    young = pd.concat(young, axis=1, sort=True)

    # reorder dataframe to correct order because of size differences
    # give rows a simple acceding value in a new column
    young["new"] = range(young.shape[0])
    # throw mean and standard error way above
    young.at["Mean", 'new'] = young.at["Mean", 'new'] + 1e6
    young.at["Standard Error", 'new'] = young.at["Standard Error", 'new'] + 1e6
    # reorder based on new column and then remove column and fix nan so writing actually works
    young = young.sort_values("new").drop('new', axis=1).replace([np.nan], "NaN").sort_index(axis=1)

    # rename indices to reflect which part data came from
    # young.index = [num_to_alpha(col_num + 1) for col_num in range(young.shape[0] - 2)] + ["Mean", "Standard Error"]

    # write file
    writer = pd.ExcelWriter(name)

    # write stress strain data onto separate sheets
    for group in groups:
        comb[group].to_excel(writer, group[0] + " " + str(group[1]), index=reading_numbers)

    # write young's data
    young.to_excel(writer, "Young's", index=True)

    # Save file
    try:
        writer.save()
    except PermissionError:
        logger.error("File Currently Open")
        return 1

    return 0


def read_complied(path: str, file_name="Complied.xlsx", reading_numbers=False) -> pd.DataFrame:
    """
    reads file already processed
    :param path: path to file
    :param file_name: name of processed file
    :param reading_numbers: were reading numbers used in writing
    :return: data for program to use
    """
    # Change file location
    try:
        chdir(path)
    except FileNotFoundError:
        logger.error("Invalid File Path")
        return 1

    # read in file
    source = pd.ExcelFile(file_name)

    # construct keys
    sheet_names = source.sheet_names
    keys = []
    for sheet in sheet_names[:-1]:
        parts = sheet.split(" ")
        keys.append(tuple([parts[0], int(parts[1])]))

    # read in data
    if not reading_numbers:
        data = [pd.io.excel.ExcelFile.parse(source, sheet) for sheet in sheet_names[:-1]] + [
            pd.io.excel.ExcelFile.parse(source, sheet_names[-1], index_col=0)]
    else:
        data = [pd.io.excel.ExcelFile.parse(source, sheet, index_col=0) for sheet in sheet_names]

    # separate young's from the rest of the data
    youngs = data.pop(-1)
    # separate the various sides in young's
    youngs_frames = []
    for name in sheet_names[:-1]:
        # separate and remove NaNs
        young = youngs.filter(regex=name).replace("NaN", np.nan).dropna(axis=0)
        # change column names
        header = []
        for head in young.columns:
            header.append(head.split(" ")[0])
        young.columns = header
        # add to list
        youngs_frames.append(young)

    # seperate stress and strain frames
    strain_frames = []
    stress_frames = []
    for frame in data:
        strain_frames.append(frame.filter(regex="Strain"))
        stress_frames.append(frame.filter(regex="Stress"))

    # construct data dict
    data = {}
    for i in range(len(strain_frames)):
        data[keys[i]] = {"Stress": stress_frames[i], "Strain": strain_frames[i], "Young's": youngs_frames[i]}

    return data


if __name__ == "__main__":
    # Assumes format of "Something #Letters.xlsx" as file name please format all files in this format
    # The something doesn't matter, the number is the general type, and the letters are a specific part
    # see readme for more details

    # Raw data folder file path
    raw_data_path = Path("C:/Users/Kieran/OneDrive/School/High School/AOS/Science/Junior Research Project/Data/Raw")

    # Compiled data file path
    compiled_data_path = Path("C:/Users/Kieran/OneDrive/School/High School/AOS/Science/Junior Research Project/Data")

    try:
        try:
            # Method of calculating strain
            method = "SUB"  # subtracting subsequent values from the first and dividing by the first
            # method = "FIXED"  # subtract all values from a fixed number and divide by that fixed number
            # pass in a value for fixed if using it
            # Reads in Data
            # change units (matches output files) if necessary
            # change area of compression/tension
            data = read(raw_data_path, method, area=200)
        except TypeError:
            logger.error("Could not read files or do calculations at read in")
            exit()

        # Processes
        complied = process(data)

        # Writes
        # Change the complied file name if you would like make sure it ends in .xlsx
        # if you want reading numbers set reading_numbers to True
        failed = write(complied, compiled_data_path)
        if failed:
            logger.error("Failed to Write")
            exit()

    except (KeyboardInterrupt, SystemExit):
        logger.error("Exiting...")

    data = read_complied(compiled_data_path)
    # Place to run stats, examples below, f_oneway is an ANOVA
    print(stats.f_oneway(data[("Side", 1)]["Young's"]["Young's"], data[("Side", 2)]["Young's"]["Young's"],
                         data[("Side", 3)]["Young's"]["Young's"]))
