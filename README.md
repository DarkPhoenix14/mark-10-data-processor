## Collecting Data
### Zeroing

RECOMENDED: Zero on the bottom of the part. This will result in the most accurate data. Use **"SUB"** method.  
NOT RECOMMENDED: Zero on the top of the part use **"FIXED"** and give the total height of the part.

### Units

All values entered into the program must be in the same units.

### Naming Data Files

Rename/Name all raw data output files to *"Something #Letters.xlsx"* like "Side 1AC.xlsx"  
 - The something doesn't matter. It is just to differentiate between yours and someone else's data if necessary or major variations in your testing groups   
 - The # represents a category, for example 10mm cubes as 1 and 20mm cubes as 2. Now works for multi-digit numbers.  
 - The letter code is just a specific part that is being tested, for example A for part A.     
   Works for multiletter codes like AC etc. These letters can be repeated for each number code.
   
None of these are hard and fast rules. The program will do a regression on all of the parts regardless as long as there is a word, number, and letters. This is just the recomended format. The letters being repeated trials is the most important for easier understanding of the spreadsheet.

---

## Running The Program

Scroll down to where it say 
```python
if __name__ == "__main__":
```
Change **raw_data_path** and **complied_data_path** file paths
    
Specifiy which method of calculating strain you would like to use   
**"SUB"** for subsequent takes all values after the first value and subtracts it from the first value and then divides by the first value    
**"FIXED"** takes all values and subtracts and then divides by a fixed value if you use this method you must specify the fixed value in the function call   
The program takes the absolute value of all strains   
    
Specify the area of compression or tension.    
These units do not have to match the raw data output units. However this will determine your units for Young's Modulus and stress.    

Specify units used if different than defaults. This is the raw data output units.   
   
Outputs to *Complied.xlsx* by default where each sheet is a type    
File has instantaneous stress, strain, and Young's Modulus for each part   
Also has instantaneous average and standard error for stress, strain, and Young's Modulus     
If you choose to change file name be sure to have it end in .xlsx      
if you want reading numbers set reading_numbers to True

The function will error on the regression but that does not matter.   

---

## Statistics

At the bottom of the file you can do stats tests on the data examples are provided.     
SciPy is used in this example but anything will work      
If you have already ran the program and have a complied data sheet you can not reread the raw data files by:      
- Comment out the code below the files paths and above stats section    
- Uncomment the *read_complied* function above the stats section

---

Email kieranhky14@gmail.com if you want editing access